package test;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.cache.query.SqlFieldsQuery;
import org.apache.ignite.cache.query.SqlQuery;
import org.apache.ignite.configuration.CacheConfiguration;

import java.util.List;
import java.util.Random;
import java.util.UUID;

public class PersonManager
{
    private final Ignite ignite;

    protected final IgniteCache<String, Person> cache;

    public PersonManager(Ignite ignite)
    {
        this.ignite = ignite;
        CacheConfiguration<String, Person> cfg = new CacheConfiguration<String, Person>("person");

        // Index the words and their counts,
        // so we can use them for fast SQL querying.
        cfg.setIndexedTypes(String.class, Person.class);
        cache = ignite.getOrCreateCache(cfg);
    }

    public void generatePersons(int n)
    {
        String[] names = new String[] {"\u006d\u0061\u006c\u0065", "\u0645\u0631\u062f"};
//        String[] names = new String[]{"مرد", "ز�s�"};
        Random randomGenerator = new Random();
        for (int i = 0; i < n; i++) {
            if (Math.random() > 0.5)
                create(new Person(names[0], randomGenerator.nextInt()));
            else
                create(new Person(names[1], randomGenerator.nextInt()));
        }
    }

    public void create(Person person)
    {
        cache.put(UUID.randomUUID().toString(), person);
    }

    public void groupby()
    {
        SqlFieldsQuery sql = new SqlFieldsQuery(
                "select name,count(name) from Person group by name;");

        List<List<?>> result = cache.query(sql).getAll();

        for (int i = 0; i < result.size(); i++) {
            System.out.println(result.get(i).get(0) + " : " + result.get(i).get(1));
        }
    }
}
