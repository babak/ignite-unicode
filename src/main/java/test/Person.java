package test;

import org.apache.ignite.cache.query.annotations.QuerySqlField;

public class Person {
    @QuerySqlField
    private String name;
    @QuerySqlField
    private int value;

    public Person(String name, int value){
        this.name = name;
        this.value = value;
    }
}
