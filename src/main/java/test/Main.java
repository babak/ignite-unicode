package test;


import org.apache.ignite.Ignite;
import org.apache.ignite.Ignition;

public class Main {
    public static void main(String[] args) {
        int n = 100;
        Ignite ignite = Ignition.start();
        PersonManager personManager = new PersonManager(ignite);

        personManager.generatePersons(n);

        ignite.compute().broadcast(() -> { 
            personManager.groupby();
        });
    }
}
